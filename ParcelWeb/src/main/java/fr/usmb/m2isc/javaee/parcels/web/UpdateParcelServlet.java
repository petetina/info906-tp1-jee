package fr.usmb.m2isc.javaee.parcels.web;


import fr.usmb.m2isc.javaee.parcels.ejb.*;
import fr.usmb.m2isc.javaee.parcels.jpa.Parcel;
import fr.usmb.m2isc.javaee.parcels.jpa.Place;
import fr.usmb.m2isc.javaee.parcels.jpa.State;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet utilisee pour mettre à jour la position et l'état d'un colis.
 */
@WebServlet("/parcel/update")
public class UpdateParcelServlet extends HttpServlet {


    private static final long serialVersionUID = 1L;
    @EJB
    private ParcelRepositoryInterface parcelRepository;
    @EJB
    private PlaceRepositoryInterface placeRepository;
    @EJB
    private StateRepositoryInterface stateRepository;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateParcelServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(
        HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Place> places = placeRepository.getAll();
        request.setAttribute("places", places);
        List<State> states = stateRepository.getAll();
        request.setAttribute("states",states);
        List<Parcel>parcels = parcelRepository.getAll();
        request.setAttribute("parcels",parcels);

        int parcelId = Integer.valueOf(request.getParameter("id"));
        Parcel p = parcelRepository.get(parcelId);
        request.setAttribute("parcel",p);
        request.getRequestDispatcher("/UpdateParcel.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
	/*@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {





	}*/

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println("in put");
        int parcelId = Integer.valueOf(request.getParameter("id"));
        int positionId = Integer.valueOf(request.getParameter("position"));
        int stateId = Integer.valueOf(request.getParameter("state"));

        Parcel p = parcelRepository.get(parcelId);
        Place pos = placeRepository.get(positionId);
        State s = stateRepository.get(stateId);

        p.setPosition(pos);
        p.setState(s);

        parcelRepository.update(p);

        response.sendRedirect("..");
    }
}
