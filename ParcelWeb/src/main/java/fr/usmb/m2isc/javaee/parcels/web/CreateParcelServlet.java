package fr.usmb.m2isc.javaee.parcels.web;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.usmb.m2isc.javaee.parcels.ejb.ParcelRepositoryInterface;
import fr.usmb.m2isc.javaee.parcels.ejb.PlaceRepositoryInterface;
import fr.usmb.m2isc.javaee.parcels.jpa.Place;

/**
 * Servlet utilisee pour creer un colis.
 */
@WebServlet("/parcel/create")
public class CreateParcelServlet extends HttpServlet{

	private static final long serialVersionUID = 5236668439173484090L;
	@EJB
	private ParcelRepositoryInterface parcelRepository;
	@EJB
	private PlaceRepositoryInterface  placeRepository;

    /**
     * @see HttpServlet#HttpServlet()
     */
	public CreateParcelServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Place> places = placeRepository.getAll();
		request.setAttribute("places", places);
		request.getRequestDispatcher("/CreateParcel.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		try {
			double weight = Double.valueOf(request.getParameter("weight"));
			double value = Double.valueOf(request.getParameter("value"));
			int originId = Integer.valueOf(request.getParameter("origin"));
			int destinationId = Integer.valueOf(request.getParameter("destination"));

			int id = parcelRepository.createParcel(value, weight, originId, destinationId);

			if (id == -1) {
				session.setAttribute("message", "An error has occured !");
				request.getRequestDispatcher("/CreateParcel.jsp").forward(request, response);
			} else {
				session.setAttribute("message", "Your parcel has been created !");
				response.sendRedirect("..");
			}
		}catch (Exception e) {
			session.setAttribute("message", "An error has occured !");
			response.sendRedirect("./create");
		}
		//System.out.println("weight = " + weight + ", value = " + value + ", originId = " + originId + ", destinationId = " + destinationId);
	}

}
