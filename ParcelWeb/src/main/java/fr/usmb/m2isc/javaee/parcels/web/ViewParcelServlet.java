package fr.usmb.m2isc.javaee.parcels.web;

import fr.usmb.m2isc.javaee.parcels.ejb.ParcelRepositoryInterface;
import fr.usmb.m2isc.javaee.parcels.jpa.Parcel;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet utilisee pour creer un colis.
 */
@WebServlet(value = "/")
public class ViewParcelServlet extends HttpServlet{

	private static final long serialVersionUID = 5236668439173484090L;
	@EJB
	private ParcelRepositoryInterface parcelRepository;

    /**
     * @see HttpServlet#HttpServlet()
     */
	public ViewParcelServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idString = request.getParameter("id");
		Parcel parcel = null;

		if(idString != null) {
			parcel = parcelRepository.get(Integer.valueOf(idString));
		}

		if(parcel == null){
			//get all
			List<Parcel> parcels = parcelRepository.getAll();
			request.setAttribute("parcels",parcels);
			request.getRequestDispatcher("/DisplayParcels.jsp").forward(request, response);
		}else{
			//get by id
			request.setAttribute("parcel", parcel);
			request.getRequestDispatcher("/DisplayParcel.jsp").forward(request, response);
		}
	}

}
