<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Mettre � jour un colis</title>
</head>
<body>
    <h1>Mettre � jour un colis</h1>
    <form action="./update?id=${parcel.id}" method="post">

        <select name="position" id="position">
            <option value="-1">S�lectionnez la position :</option>
            <c:forEach items="${places}"  var="place">


                <c:choose>
                    <c:when test="${place.id == parcel.position.id}">
                        <option value="${place.id}" selected>${place.name}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${place.id}">${place.name}</option>
                    </c:otherwise>
                </c:choose>

            </c:forEach>
        </select>
        <br/>
        <select name="state" id="state">
            <option value="-1">S�lectionner l'�tat :</option>
            <c:forEach items="${states}"  var="state">

                <c:choose>
                    <c:when test="${state.id == parcel.state.id}">
                        <option value="${state.id}" selected>${state.name}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${state.id}">${state.name}</option>
                    </c:otherwise>
                </c:choose>


            </c:forEach>
        </select>

        <br/>
        <input type="submit" value="valider"/>
    </form>                                                                                                                                                                                                                                                             </form>
    <p><a href="../parcel">Revenir � la page principale</a></p>
</body>
</html>
