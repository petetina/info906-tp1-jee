<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <title>Display parcel</title>
        <link rel="stylesheet" type="text/css" href="css/base.css"/>
    </head>
    <body>
    <h1>Display parcel</h1>
    <a href="./parcel/update?id=${parcel.id}">Edit</a>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>State</th>
            <th>Value</th>
            <th>Weight</th>
            <th>Origin</th>
            <th>Destination</th>
            <th>Current position</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td>${parcel.id }</td>
                <td>${parcel.state.toString()}</td>
                <td>${parcel.value}</td>
                <td>${parcel.weight}</td>
                <td>${parcel.origin.toString()}</td>
                <td>${parcel.destination.toString()}</td>
                <td>${parcel.position.toString()}</td>
            </tr>
        </tbody>
    </table>

    <p><a href="./">Revenir � la page principale</a></p>
    </body>
</html>