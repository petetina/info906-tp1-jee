<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		 pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Create a parcel</title>
		<link rel="stylesheet" type="text/css" href="css/base.css"/>
	</head>
	<body>
	<h1>Create a parcel</h1>
	<%= session.getAttribute("message")== null?"":session.getAttribute("message") %>
	<%
		session.removeAttribute("message");
	%>
	<form action="./create" method="post">
		<label>Poids : </label><input type="text" id="weight" name="weight"><br/>
		<label>Valeur : </label><input type="text" id="value" name="value"><br/>
		<select id="origin" name="origin">
			<option value="-1" disabled>S�lectionnez l'origine :</option>
			<c:forEach items="${places}"  var="place">
				<option value="${place.id}">${place.name}</option>
			</c:forEach>
		</select>
		<br/>
		<select id="destination" name="destination">
			<option value="-1" disabled>S�lectionner la destination :</option>
			<c:forEach items="${places}"  var="place">
				<option value="${place.id}">${place.name}</option>
			</c:forEach>
		</select>
		<br/>
		<input type="submit" value="valider"/>
	</form>                                                                                                                                                                                                                                                             </form>
	<p><a href="../parcel">Revenir � la page principale</a></p>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript">
        $('#origin option[value="-1"]').attr('selected',true);
        $('#destination option[value="-1"]').attr('selected',true);
        $('#destination').attr('disabled',true);

        $('#origin').on('change',function(){
            console.log(($('#destination').attr('disabled')));
            //alert(this.value);
            $('#destination option').each(function (index) {
                if ($(this).val() != -1)
                    $(this).removeAttr('disabled');
            });
            $('#destination').removeAttr('disabled');
            $('#destination option[value="' + this.value + '"]').attr('disabled', true);
        });

        $('#destination').on('change',function(){
            $('#origin option').each(function (index) {
                if ($(this).val() != -1)
                    $(this).removeAttr('disabled');
            });
            $('#origin option[value="' + $(this).val() + '"]').attr('disabled', true);
        });

        $('form').submit(function(){
            var weight = parseFloat($('#weight').val());
            var value = parseFloat($('#value').val());
            return !isNaN(weight) &&  weight > 0
                && !isNaN(value) &&  value > 0;

        });
	</script>
	</body>
</html>