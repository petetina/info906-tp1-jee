# TP1 Gestion d'un colis JavaEE 7

Pour faire simple l'application consiste à manipuler des colis.

##How to run

- Mettre la base de données du serveur d'application en route (Glassfish)
`./asadmin start-database` dans le dossier glassfish5/bin/
- Lancer ParcelEar puis taper l'url  dans votre navigateur

##Serveur d'application
Glassfish 5.0.1

##Classe persistante
- Parcel : représente un colis par son poids, sa valeur, son origine, sa destination, son état et sa position courante.
- Place : représente un lieu par son nom et ses coordonnées GPS. 
- State : représente un état : 
    - Enregistré
    - En attente
    - En acheminement
    - Bloqué
    - Livré


##EJB
- GoldenRepository : Enregistre des données dans la base de données
- Repository : Classe abstraite contenant le CRUD
- RepositoryInterface : Interface du CRUD
- ParcelRepository : DAO de la classe Parcel
- ParcelRepositoryInterface : Interface du DAO de la classe Parcel 
- PlaceRepository : DAO de la classe Place
- PlaceRepositoryInterface : Interface du DAO de la classe Place
- StateRepository : DAO de la classe State
- StateRepositoryInterface : Interface du DAO de la classe State


##Servlets

- CreateParcelServlet : Traitement de création de colis
- UpdateParcelServlet : Traitement de mise à jour de colis (modification de la position et de l'état)
- ViewParcelServlet : Traitement d'affichage du parcel

##JSP

- CreateParcel : formulaire de création
- DisplayParcels : vue de tous les colis
- DisplayParcel : vue d'un colis en particulier
- UpdateParcel : Formulaire de modification

