package fr.usmb.m2isc.javaee.parcels.ejb;

import fr.usmb.m2isc.javaee.parcels.jpa.State;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Remote
@Stateless
public class StateRepository extends Repository<State> implements StateRepositoryInterface{

    @PersistenceContext
    private EntityManager em;

    public StateRepository() {
        super(State.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public State getByName(String name){
        return (State)em.createQuery("SELECT s FROM State s WHERE s.name = '" + name + "'").getSingleResult();
    }

}
