package fr.usmb.m2isc.javaee.parcels.ejb;

import java.util.List;

public interface RepositoryInterface<T> {
    void create(T entity);
    void update(T entity);
    void remove(T entity);
    T get(Object id);
    List<T> getAll();
    int count();
}