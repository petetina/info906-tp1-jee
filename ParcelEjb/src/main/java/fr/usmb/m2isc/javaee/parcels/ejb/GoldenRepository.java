package fr.usmb.m2isc.javaee.parcels.ejb;

import fr.usmb.m2isc.javaee.parcels.jpa.Place;
import fr.usmb.m2isc.javaee.parcels.jpa.State;

import javax.annotation.PostConstruct;
import javax.ejb.*;

@Startup
@Singleton
@Stateless
@Remote
public class GoldenRepository {

	@EJB
	private PlaceRepositoryInterface placeRepository;
	@EJB
	private ParcelRepositoryInterface parcelRepository;
	@EJB
	private StateRepositoryInterface stateRepository;


	public GoldenRepository() {
	}

	@PostConstruct
	private void init(){
		placeRepository.create(new Place("La Poste (Le bourget-du-Lac)",45.649263, 5.859640));
		placeRepository.create(new Place("La Poste (Aix-les-bains)",45.6922504,5.90129));

		stateRepository.create(new State("Enregistré"));
		stateRepository.create(new State("En attente"));
		stateRepository.create(new State("En acheminement"));
		stateRepository.create(new State("Bloqué"));
		stateRepository.create(new State("Livré"));

		parcelRepository.createParcel(1.0,2.3,1,2);

	}


}
