package fr.usmb.m2isc.javaee.parcels.ejb;

import fr.usmb.m2isc.javaee.parcels.jpa.Place;

public interface PlaceRepositoryInterface extends RepositoryInterface<Place>{
    //It's redundant to extend RepositoryInterface

}