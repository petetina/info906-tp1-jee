package fr.usmb.m2isc.javaee.parcels.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class State implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    @Column(unique=true)
    private String name;

    public State() {
    }

    public int getId() {
        return id;
    }

    public State(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
