package fr.usmb.m2isc.javaee.parcels.ejb;


import fr.usmb.m2isc.javaee.parcels.jpa.Parcel;


public interface ParcelRepositoryInterface extends RepositoryInterface<Parcel>{

    int createParcel(double value, double weight, int originId, int destinationId);

}