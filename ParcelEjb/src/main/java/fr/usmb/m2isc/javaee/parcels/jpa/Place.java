package fr.usmb.m2isc.javaee.parcels.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Place implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id_place")
    private int id;
    private double latitude;
    private double longitude;
    private String name;

    public Place() {
    }

    public Place(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return  name + "<br/>\n" +
                "(" + latitude + "," + longitude + ")";
    }
}
