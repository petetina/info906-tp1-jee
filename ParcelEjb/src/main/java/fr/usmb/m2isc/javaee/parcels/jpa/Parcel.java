package fr.usmb.m2isc.javaee.parcels.jpa;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Entity
public class Parcel implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    @NotNull
    private double weight;
    @NotNull
    private double value;
    @ManyToOne
    private Place origin;
    @ManyToOne
    private Place destination;

    @ManyToOne
    private Place position;
    @NotNull
    @ManyToOne
    private State state;

    public Parcel() {
    }

    public Parcel(double weight, double value) {
        this.weight = weight;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public double getWeight() {
        return weight;
    }

    public double getValue() {
        return value;
    }

    public Place getOrigin() {
        return origin;
    }

    public Place getDestination() {
        return destination;
    }

    public void setOrigin(Place origin) {
        this.origin = origin;
    }

    public void setDestination(Place destination) {
        this.destination = destination;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Place getPosition() {
        return position;
    }

    public void setPosition(Place position) {
        this.position = position;
    }

}
