package fr.usmb.m2isc.javaee.parcels.ejb;

import fr.usmb.m2isc.javaee.parcels.jpa.Place;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@Remote
public class PlaceRepository extends Repository<Place> implements PlaceRepositoryInterface{

	@PersistenceContext
	private EntityManager em;

	public PlaceRepository() {
		super(Place.class);
	}

	//Usefull we calling super class methods !
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
